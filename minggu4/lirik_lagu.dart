import 'dart:async';

void main(List<String> args) {
  print("Nyanyi yukkkk  !!!");

// Nama : Achmad Sirojudin
// NIM : E41201607
// Judul lagu = Bertaut - Nadin Amizah
  var lirik = [
    'Bun, Hidup berjalan seperti bajingan',
    'Seperti landak yang tak punya teman',
    'Dia menggonggong bak suara hujan',
    'Dan kau pangeranku mengambil peran',
    '-----------------------------------',
    'Bun, Kalau saat hancur ku disayang',
    'Apalagi saat ku jadi juara',
    'Saat tahu arah kau disana',
    'Menjadi gagah saat ku tak bisa',
    '-----------------------------------',
    'Sedikit kujelaskan tentangku dan kamu',
    'Agar seisi dunia tau',
    '-----------------------------------',
    'Keras kepalaku sama denganmu',
    'Caraku marah, caraku tersenyum',
    'Seperti detak jantung yang bertaut',
    'Nyawaku nyala karena denanmu',
    '-----------------------------------',
    'Aku masih ada samapai disini',
    'Melihatmu kuat setengah mati',
    'Seperti detak jantung yang bertaut',
    'Nyawaku nyala karena denanmu',
  ];

  var timer = 5;
  Timer(Duration(seconds: 3), () => print(lirik[0]));

  for (var i = 0; i < lirik.length; i++) {
    Timer(Duration(seconds: timer), () => print(lirik[i]));
    timer += 5;
  }
}
