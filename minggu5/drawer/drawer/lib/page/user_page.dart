import 'package:flutter/material.dart';

class UserPage extends StatelessWidget {
  final String nama;
  final String urlImage;

  const UserPage({
    Key? key,
    required this.nama,
    required this.urlImage,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.pink,
          title: Text(nama),
          centerTitle: true,
        ),
        body: Image.network(
          urlImage,
          width: double.infinity,
          height: double.infinity,
          fit: BoxFit.cover,
        ),
      );
}
