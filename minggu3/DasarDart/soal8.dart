import 'dart:io';

void main() {
  var tgl = 28;
  var bln = 4;
  var thn = 2002;

  if (tgl <= 0 ||
      tgl > 31 && bln <= 0 ||
      bln >= 13 && thn < 1900 ||
      thn > 2200) {
    print("Invalid");
  } else {
    switch (bln) {
      case 1:
        {
          print("${tgl.toString()} Januari ${thn.toString()}");
          break;
        }
      case 2:
        {
          print("${tgl.toString()} Februari ${thn.toString()}");
          break;
        }
      case 3:
        {
          print("${tgl.toString()} Maret ${thn.toString()}");
          break;
        }
      case 4:
        {
          print("${tgl.toString()} April ${thn.toString()}");
          break;
        }
      case 5:
        {
          print("${tgl.toString()} Mei ${thn.toString()}");
          break;
        }
      case 6:
        {
          print("${tgl.toString()} Juni ${thn.toString()}");
          break;
        }
      case 7:
        {
          print("${tgl.toString()} Juli ${thn.toString()}");
          break;
        }
      case 8:
        {
          print("${tgl.toString()} Agustus ${thn.toString()}");
          break;
        }
      case 9:
        {
          print("${tgl.toString()} September ${thn.toString()}");
          break;
        }
      case 10:
        {
          print("${tgl.toString()} Oktober ${thn.toString()}");
          break;
        }
      case 11:
        {
          print("${tgl.toString()} November ${thn.toString()}");
          break;
        }
      case 12:
        {
          print("${tgl.toString()} Desember ${thn.toString()}");
          break;
        }
    }
  }
}
