import 'dart:io';

void main(List<String> args) {
  print("Masukkan nama anda!");

  var nama = stdin.readLineSync();

  if (nama == "") {
    print("Nama tidak boleh kosong!");
  } else {
    print(
        "Pilih peran anda dengan mengetikkan angka ! \n1. Penyihir \n2. Guard \n3. Werewplf");
    var peran = stdin.readLineSync();

    if (peran == "") {
      print("Peran tidak boleh kosong!");
    } else if (peran == "1") {
      print("Selamat datang di Dunia Werewolf, $nama");
      print(
          "Halo penyihir $nama, kamu dapat melihat siapa yang menjadi werewolf!");
    } else if (peran == "2") {
      print("Selamat datang di Dunia Werewolf, $nama");
      print(
          "Halo Guard $nama, kamu akan membantu melindungi temanmu dari serangan.");
    } else if (peran == "3") {
      print("Selamat datang di Dunia Proxytia, $nama");
      print("Halo $nama, ciptakan kamu akan memakan mangsa setiap malam!");
    } else {
      print("Peran tidak ditemukan!");
    }
  }
}
