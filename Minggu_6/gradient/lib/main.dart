import 'package:flutter/material.dart';

void main() {
  runApp(new MaterialApp(
    debugShowCheckedModeBanner: false,
    home: new Home(),
  ));
}

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);
  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        body: new Column(children: <Widget>[
      Flexible(
          flex: 1,
          child: Container(
            decoration: new BoxDecoration(
                gradient: LinearGradient(
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                    colors: [
                  Color.fromARGB(255, 210, 243, 240),
                  Color.fromARGB(255, 45, 82, 115)
                ])),
          )),
      Flexible(
          flex: 1,
          child: Container(
            decoration: new BoxDecoration(
                gradient: SweepGradient(
                    colors: [Colors.green.shade700, Colors.greenAccent])),
          )),
      Flexible(
        flex: 1,
        child: Container(
          decoration: BoxDecoration(
              gradient: RadialGradient(radius: 1.0, colors: [
            Color.fromARGB(255, 194, 220, 26),
            Color.fromARGB(255, 249, 132, 37)
          ])),
        ),
      )
    ]));
  }
}
